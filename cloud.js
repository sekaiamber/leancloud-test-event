var AV = require('leanengine');

// 随机抽奖
const awardsCount = {
  1: 1000,
  2: 100,
  3: 30,
  4: 30,
  5: 2,
  6: 1,
};

function getTodayZero(d = new Date()) {
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

// 得奖率不够的时候，适当调小这个值
const max = 2000;
// let bi = 60;

let last = (new Date().getMilliseconds() / 1000).toFixed(3).slice(2);

function randomAward() {
  const now = (new Date().getMilliseconds() / 1000).toFixed(3).slice(2);
  
  const high = parseInt(parseFloat('0.' + now + last) * max, 10);
  last = now;

  let you = 0;
  let award = 0;


  const k = Object.keys(awardsCount);
  for (let i = 0; i < k.length; i += 1) {
    you += awardsCount[k[i]];
    if (you >= high) {
      award = parseInt(k[i], 10);
      break;
    }
  }

  // console.log('award --:', high, you, award);

  // bi -= 1;
  // console.log('award --:', bi);
  // if (bi === 0) {
  //   award = 5;
  // }

  const querya = new AV.Query('awards');
  querya.equalTo('type', award);
  return querya.find().then((results) => {
    if (results.length === 0) return 0;
    const awardResult = results[0];
    if (awardResult.attributes.count > 0) {
      awardResult.increment('count', -1);
      awardResult.save();
    } else {
      return 0;
    }
    return award;
  });
}

function getRandomAwards(userId) {
  const query = new AV.Query('UserAwards');
  query.equalTo('userId', userId);
  const todayZero = getTodayZero();
  query.greaterThanOrEqualTo('createdAt', todayZero);
  return query.find().then((awards) => {
      if (awards.length > 0) {
          if (awards.length === 1) {
              const userQuery = new AV.Query('MyUser');
              return userQuery.get(userId).then((user) => {
                  const sharedAt = user.attributes.sharedAt;
                if (sharedAt && getTodayZero(sharedAt).getTime() === todayZero.getTime()) {
                    return randomAward();
                }
                return Promise.reject('UA_TODAY_CAN_SHARE');
              });
          }
          return Promise.reject('UA_TODAY');
      }
      return randomAward();
  });

}

// 保存物件
function SaveObject(tabelName, value) {
  const TableObject = AV.Object.extend(tabelName);
  const obj = new TableObject();
  Object.keys(value).forEach((k) => {
    obj.set(k, value[k]);
  });
  return obj.save();
}


AV.Cloud.define('ifUserExsit', function(request) {
  const query = new AV.Query('MyUser');
return query.get(request.params.userId);
});


AV.Cloud.define('getUserBlobCount', function(request) {
  const userid = request.params.userId;
  const userQuery = new AV.Query('UserBlob');
  userQuery.equalTo('userId', userid);
  return userQuery.count();
});



AV.Cloud.define('getUserBlobs', async (request) => {
  const userid = request.params.userId;
  const query = new AV.Query('UserBlob');
  query.equalTo('verify', true);
  query.limit(40);
  query.descending('updatedAt');
  let normal = await query.find();
  normal = normal.map((blob) => ({
    ...blob.attributes,
    objectId: blob.id,
    createdAt: blob.createdAt,
    updatedAt: blob.updatedAt,
  }));
  if (userid && userid.length > 0) {
    const userQuery = new AV.Query('UserBlob');
    userQuery.equalTo('userId', userid);
    userQuery.descending('updatedAt');
    let userBlob = await userQuery.find();
    userBlob = userBlob.map((blob) => ({
      ...blob.attributes,
      objectId: blob.id,
      createdAt: blob.createdAt,
      updatedAt: blob.updatedAt,
    }));
    normal = userBlob.concat(normal);
  }
  normal = normal.slice(0, 40);
  return normal;
});



AV.Cloud.define('userBallot_old', function(request) {
  const userId = request.params.userId;
return getRandomAwards(userId).then((result) => {
    SaveObject('UserAwards', {
        userId,
        award: result,
    })
    return result;
});
});



AV.Cloud.define('createUser', function(request) {
  const rel = request.params.rel;
  return SaveObject('MyUser', {
    name: '',
    phone: '',
    address: '',
    awardsCount: 5,
    rel,
})
});


AV.Cloud.define('sendMessage', function(request) {
  const id = request.params.userId;
const message = request.params.message;

return SaveObject('UserBlob', {
  type: 0,
  userId: id,
  verify: false,
  message,
});
});



AV.Cloud.define('uploadImage', function(request) {
  const id = request.params.userId;
const image = request.params.image;

return SaveObject('UserBlob', {
  type: 1,
  userId: id,
  verify: false,
  image,
});
});


AV.Cloud.define('getUserAwards', function(request) {
  const userid = request.params.userId;
const query = new AV.Query('UserAwards');
query.equalTo('userId', userid);
query.limit(40);
query.descending('createdAt');
return query.find();
});



AV.Cloud.define('updateUser', function(request) {
  // 第一个参数是 className，第二个参数是 objectId
const user = AV.Object.createWithoutData('MyUser', request.params.userId);
// 修改属性
if (request.params.name) {
    user.set('name', request.params.name);
}
if (request.params.phone) {
    user.set('phone', request.params.phone);
}
if (request.params.address) {
    user.set('address', request.params.address);
}
if (request.params.sharedAt) {
  user.set('sharedAt', request.params.sharedAt);
}
// 保存到云端
return user.save();
});



AV.Cloud.define('getUnverifiedUserBlobs', function(request) {
  const query = new AV.Query('UserBlob');
query.equalTo('verify', false);
query.limit(20);
query.descending('createdAt');
return query.find();
});
AV.Cloud.define('verifyUserBlob', function(request) {
  // 第一个参数是 className，第二个参数是 objectId
const user = AV.Object.createWithoutData('UserBlob', request.params.objectId);
// 修改属性
user.set('verify', true);
// 保存到云端
return user.save();
});



AV.Cloud.define('adminLogin', function(request) {
  const name = request.params.username;
const password = request.params.password;

return name === 'pinganadmin' && password === 'tp@sA7e$';
});



AV.Cloud.define('deleteUserBlob', function(request) {
  const todo = AV.Object.createWithoutData('UserBlob', request.params.objectId);
return todo.destroy();
});


AV.Cloud.define('queryUser', function(request) {
  const query = new AV.Query('MyUser');
query.equalTo(request.params.type, request.params.value);
return query.find();
});

async function checkUserHasAward(user) {
  const userId = user.id;
  if (user.createdAt < new Date('2017-12-25 00:00:00')) {
    return true;
  }
  const query = new AV.Query('UserAwards');
  query.equalTo('userId', userId);
  query.greaterThanOrEqualTo('award', 1);
  const count = await query.count();
  return count > 0;
}

async function saveUserBallot(userId, award) {
  SaveObject('UserAwards', {
    userId,
    award,
  });
}

function wrapAward(award, message) {
  return {
    award,
    message,
  }
}

AV.Cloud.define('userBallot', async (request) => {
  const userId = request.params.userId;
  // 获得用户剩余原生数量
  const user = await new AV.Query('MyUser').get(userId);
  const userAwardsCount = user.attributes.awardsCount;
  // 获取用户今天是否抽奖
  const query = new AV.Query('UserAwards');
  query.equalTo('userId', userId);
  const todayZero = getTodayZero();
  query.greaterThanOrEqualTo('createdAt', todayZero);
  const todayAwards = await query.count();
  // 用户原生未用完
  if (userAwardsCount > 0) {
    if (todayAwards > 0) {
      // 今天已经抽过
      return Promise.reject('UA_TODAY');
    }
    // 若是最后一次抽奖，则提示分享
    const message = userAwardsCount === 1 ? 'T_NORMAL_SHARE' : 'T_NORMAL';
    // 判断用户是否已经中过奖
    const userHasAward = await checkUserHasAward(user);
    let award = 0;
    if (!userHasAward) {
      // 中过奖就直接存0，否则真实抽奖
      award = await randomAward();
    }
    await saveUserBallot(userId, award);
    user.increment('awardsCount', -1);
    await user.save();
    return wrapAward(award, message);
  }
  // 用户原生用完了
  if (todayAwards > 0) {
    // 今天已经抽过
    const queryTotalAwards = new AV.Query('UserAwards');
    queryTotalAwards.equalTo('userId', userId);
    const userAwardsCount = await queryTotalAwards.count();
    if (userAwardsCount > 5) {
      // 今天已经抽过了，提示明天分析
      return Promise.reject('UA_TOMORROW_SHARE');
    }
    // 意味着是首次分享，能抽
  }
  // 检查分享情况
  const sharedAt = user.attributes.sharedAt;
  if (sharedAt && getTodayZero(sharedAt).getTime() === todayZero.getTime()) {
    // 今日分享过了
    // 判断用户是否已经中过奖
    const userHasAward = await checkUserHasAward(user);
    let award = 0;
    if (!userHasAward) {
      // 中过奖就直接存0，否则真实抽奖
      award = await randomAward();
    }
    await saveUserBallot(userId, award);
    return wrapAward(award, 'T_NORMAL');
  }
  // 今天还没抽过，提示分享
  return Promise.reject('UA_TODAY_SHARE');
})

/**
 * userBallot
 * ERROR:
 * 
 * UA_TODAY: 今天已经抽过，请明天来抽
 * UA_TODAY_SHARE: 今天还未分享，分享以后能抽
 * UA_TOMORROW_SHARE: 今日已经抽过，请明天来分享
 * 
 * TEXT:
 * T_NORMAL: 正常得奖词
 * T_NORMAL_SHARE: 得奖词，并引导分享
 */

const maxTimeDiff = 7200 * 1000;
const getTicket = require('./wx').getTicket;
const getAccessToken = require('./wx').getAccessToken;
const sign = require('./wx').sign;

AV.Cloud.define('getWxConfig', async (request) => {
  const url = request.params.url;
  const query = new AV.Query('jsTicket');
  const ticketObj = await query.first();
  const timeDiff = new Date() - ticketObj.updatedAt;
  if (timeDiff > maxTimeDiff) {
    const ticket = await getTicket();
    ticketObj.set('ticket', ticket);
    await ticketObj.save();
    return sign(ticket, url);
  }
  return sign(ticketObj.attributes.ticket, url);
});

AV.Cloud.define('getKol', async () => {
  const query = new AV.Query('kol');
  let kols = await query.find();
  kols = kols.map(k => ({
    objectId: k.id,
    name: k.attributes.name,
  }));
  for (let i = 0; i < kols.length; i++) {
    const kol = kols[i];
    const uquery = new AV.Query('MyUser');
    uquery.equalTo('rel', kol.objectId);
    const count = await uquery.count();
    kol.count = count;
  }
  return kols;
});

