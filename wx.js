const https = require('https');

const appId = 'wxdfe11317aab9b28b';
const appSecret = 'ad3581e5d449b22d81bb437c2bd8c18a';

function httpsGet(url) {
  return new Promise((resolve) => {
    https.get(url, (res) => {
      res.on('data', (buf) => {
        resolve(buf.toString());
      });
    });
  });
}

async function getAccessToken() {
  const resp = await httpsGet(`https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=${appId}&secret=${appSecret}`);
  return JSON.parse(resp).access_token;
}

async function getTicket() {
  const token = await getAccessToken();
  const resp = await httpsGet(`https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=${token}&type=jsapi`);
  return JSON.parse(resp).ticket;
}

// const ticket = 'sM4AOVdWfPE4DxkXGEs8VISAMjlPk_FrlRwpe4LNTlHEU-jzf1284hibuPhCm4kQGYvM0AfoxRkROAcXZhMyYA';

var createNonceStr = function () {
  return Math.random().toString(36).substr(2, 15);
};

var createTimestamp = function () {
  return parseInt(new Date().getTime() / 1000) + '';
};

var raw = function (args) {
  var keys = Object.keys(args);
  keys = keys.sort()
  var newArgs = {};
  keys.forEach(function (key) {
    newArgs[key.toLowerCase()] = args[key];
  });

  var string = '';
  for (var k in newArgs) {
    string += '&' + k + '=' + newArgs[k];
  }
  string = string.substr(1);
  return string;
};

/**
* @synopsis 签名算法 
*
* @param jsapi_ticket 用于签名的 jsapi_ticket
* @param url 用于签名的 url ，注意必须动态获取，不能 hardcode
*
* @returns
*/
function sign(jsapi_ticket, url) {
  var ret = {
    jsapi_ticket: jsapi_ticket,
    nonceStr: createNonceStr(),
    timestamp: createTimestamp(),
    url: url.split('#')[0]
  };
  var string = raw(ret);
      jsSHA = require('jssha');
      shaObj = new jsSHA(string, 'TEXT');
  ret.signature = shaObj.getHash('SHA-1', 'HEX');
  ret.appId = appId;

  return ret;
};

module.exports = {
  getAccessToken,
  getTicket,
  sign,
};

