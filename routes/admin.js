'use strict';
var router = require('express').Router();
var AV = require('leanengine');

var Todo = AV.Object.extend('Todo');

// 查询 Todo 列表
router.get('/', function(req, res, next) {
  res.render('todos', {
    title: 'TODO 列表',
    todos: []
  });
});


module.exports = router;
