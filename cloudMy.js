var AV = require('leanengine');

// 随机抽奖
const awardsCount = {
  1: 1000,
  2: 100,
  3: 30,
  4: 30,
  5: 2,
  6: 1,
};

function getTodayZero(d = new Date()) {
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
}

// 得奖率不够的时候，适当调小这个值
const max = 5000;

function randomAward() {
    const high = parseInt(max * Math.random(), 10);
      let you = 0;
      let award = 0;
    
      const k = Object.keys(awardsCount);
      for (let i = 0; i < k.length; i += 1) {
        you += awardsCount[k[i]];
        if (you >= high) {
          award = parseInt(k[i], 10);
          break;
        }
      }
    
      const querya = new AV.Query('awards');
      querya.equalTo('type', award);
      return querya.find().then((results) => {
        if (results.length === 0) return 0;
        const awardResult = results[0];
        if (awardResult.attributes.count > 0) {
          awardResult.increment('count', -1);
          awardResult.save();
        } else {
          return 0;
        }
        return award;
      });
}

function getRandomAwards(userId) {
  const query = new AV.Query('UserAwards');
  query.equalTo('userId', userId);
  const todayZero = getTodayZero();
  query.greaterThanOrEqualTo('createdAt', todayZero);
  return query.find().then((awards) => {
      if (awards.length > 0) {
          if (awards.length === 1) {
              const userQuery = new AV.Query('MyUser');
              return userQuery.get(userId).then((user) => {
                  const sharedAt = user.attributes.sharedAt;
                if (sharedAt && getTodayZero(sharedAt).getTime() === todayZero.getTime()) {
                    return randomAward();
                }
                return Promise.reject('UA_TODAY_CAN_SHARE');
              });
          }
          return Promise.reject('UA_TODAY');
      }
      return randomAward();
  });

}

// 保存物件
function SaveObject(tabelName, value) {
  const TableObject = AV.Object.extend(tabelName);
  const obj = new TableObject();
  Object.keys(value).forEach((k) => {
    obj.set(k, value[k]);
  });
  return obj.save();
}


AV.Cloud.define('ifUserExsit', function(request) {
  const query = new AV.Query('MyUser');
return query.get(request.params.userId);
});
AV.Cloud.define('getUserBlobs', function(request) {
  // const userid = request.params.userId;
const query = new AV.Query('UserBlob');
query.equalTo('verify', true);
query.limit(40);
query.descending('createdAt');
return query.find();
});
AV.Cloud.define('userBallot', function(request) {
  const userId = request.params.userId;
return getRandomAwards(userId).then((result) => {
    SaveObject('UserAwards', {
        userId,
        award: result,
    })
    return result;
});
});
AV.Cloud.define('createUser', function(request) {
  return SaveObject('MyUser', {
    name: '',
    phone: '',
    address: ''
})
});
AV.Cloud.define('sendMessage', function(request) {
  const id = request.params.userId;
const message = request.params.message;

return SaveObject('UserBlob', {
  type: 0,
  userId: id,
  verify: false,
  message,
});
});
AV.Cloud.define('uploadImage', function(request) {
  const id = request.params.userId;
const image = request.params.image;

return SaveObject('UserBlob', {
  type: 1,
  userId: id,
  verify: false,
  image,
});
});
AV.Cloud.define('getUserAwards', function(request) {
  const userid = request.params.userId;
const query = new AV.Query('UserAwards');
query.equalTo('userId', userid);
query.limit(40);
query.descending('createdAt');
return query.find();
});
AV.Cloud.define('updateUser', function(request) {
  // 第一个参数是 className，第二个参数是 objectId
const user = AV.Object.createWithoutData('MyUser', request.params.userId);
// 修改属性
if (request.params.name) {
    user.set('name', request.params.name);
}
if (request.params.phone) {
    user.set('phone', request.params.phone);
}
if (request.params.address) {
    user.set('address', request.params.address);
}
// 保存到云端
return user.save();
});
AV.Cloud.define('getUnverifiedUserBlobs', function(request) {
  const query = new AV.Query('UserBlob');
query.equalTo('verify', false);
query.limit(20);
query.descending('createdAt');
return query.find();
});
AV.Cloud.define('verifyUserBlob', function(request) {
  // 第一个参数是 className，第二个参数是 objectId
const user = AV.Object.createWithoutData('UserBlob', request.params.objectId);
// 修改属性
user.set('verify', true);
// 保存到云端
return user.save();
});
AV.Cloud.define('adminLogin', function(request) {
  const name = request.params.username;
const password = request.params.password;

return name === 'pinganadmin' && password === 'tp@sA7e$';
});
AV.Cloud.define('deleteUserBlob', function(request) {
  const todo = AV.Object.createWithoutData('UserBlob', request.params.objectId);
return todo.destroy();
});
AV.Cloud.define('queryUser', function(request) {
  const query = new AV.Query('MyUser');
query.equalTo(request.params.type, request.params.value);
return query.find();
});